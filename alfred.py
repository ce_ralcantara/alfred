# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'alfred.ui'
#
# Created: Sat Nov  5 12:01:08 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(364, 267)
        self.timer_progress_bar = QtWidgets.QProgressBar(Dialog)
        self.timer_progress_bar.setGeometry(QtCore.QRect(10, 140, 341, 61))
        font = QtGui.QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.timer_progress_bar.setFont(font)
        self.timer_progress_bar.setProperty("value", 24)
        self.timer_progress_bar.setObjectName(_fromUtf8("timer_progress_bar"))
        self.timer_remaining_time = QtWidgets.QLabel(Dialog)
        self.timer_remaining_time.setGeometry(QtCore.QRect(10, 30, 341, 111))
        font = QtGui.QFont()
        font.setPointSize(72)
        font.setBold(True)
        font.setWeight(75)
        self.timer_remaining_time.setFont(font)
        self.timer_remaining_time.setAlignment(QtCore.Qt.AlignCenter)
        self.timer_remaining_time.setObjectName(_fromUtf8("timer_remaining_time"))
        self.timer_btn_start = QtWidgets.QPushButton(Dialog)
        self.timer_btn_start.setGeometry(QtCore.QRect(10, 210, 341, 27))
        self.timer_btn_start.setObjectName(_fromUtf8("timer_btn_start"))
        self.timer_btn_stop = QtWidgets.QPushButton(Dialog)
        self.timer_btn_stop.setGeometry(QtCore.QRect(10, 210, 341, 27))
        self.timer_btn_stop.setObjectName(_fromUtf8("timer_btn_stop"))
        self.time_select = QtWidgets.QSpinBox(Dialog)
        self.time_select.setGeometry(QtCore.QRect(100, 40, 161, 91))
        font = QtGui.QFont()
        font.setPointSize(72)
        self.time_select.setFont(font)
        self.time_select.setMinimum(1)
        self.time_select.setMaximum(60)
        self.time_select.setProperty("value", 25)
        self.time_select.setObjectName(_fromUtf8("time_select"))
        self.title_message = QtWidgets.QLabel(Dialog)
        self.title_message.setGeometry(QtCore.QRect(30, 10, 301, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.title_message.setFont(font)
        self.title_message.setAlignment(QtCore.Qt.AlignCenter)
        self.title_message.setObjectName(_fromUtf8("title_message"))
        self.timer_interrupted = QtWidgets.QLabel(Dialog)
        self.timer_interrupted.setGeometry(QtCore.QRect(10, 25, 341, 111))
        font = QtGui.QFont()
        font.setPointSize(38)
        font.setBold(True)
        font.setWeight(75)
        self.timer_interrupted.setFont(font)
        self.timer_interrupted.setAlignment(QtCore.Qt.AlignCenter)
        self.timer_interrupted.setObjectName(_fromUtf8("timer_interrupted"))
        self.alfred_toggle_focused = QtWidgets.QLabel(Dialog)
        self.alfred_toggle_focused.setGeometry(QtCore.QRect(10, 30, 341, 110))
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.alfred_toggle_focused.setFont(font)
        self.alfred_toggle_focused.setAlignment(QtCore.Qt.AlignCenter)
        self.alfred_toggle_focused.setObjectName(_fromUtf8("alfred_toggle_focused"))
        self.alfred_toggle_shortcut = QtWidgets.QLabel(Dialog)
        self.alfred_toggle_shortcut.setGeometry(QtCore.QRect(30, 240, 301, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.alfred_toggle_shortcut.setFont(font)
        self.alfred_toggle_shortcut.setAlignment(QtCore.Qt.AlignCenter)
        self.alfred_toggle_shortcut.setObjectName(_fromUtf8("alfred_toggle_shortcut"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.timer_remaining_time.setText(_translate("Dialog", "25:00", None))
        self.timer_btn_start.setText(_translate("Dialog", "Start", None))
        self.timer_btn_stop.setText(_translate("Dialog", "Interrupt", None))
        self.title_message.setText(_translate("Dialog", "Everyone knows your time is valuable!", None))
        self.timer_interrupted.setText(_translate("Dialog", "Interrupted", None))
        self.alfred_toggle_focused.setText(_translate("Dialog", "FOCUSED", None))
        self.alfred_toggle_shortcut.setText(_translate("Dialog", "Alfred Toggle: Ctrl+Alt+A", None))

