import usb.core
import usb.util

class LuxaforPyUSB() :
    def __init__(self):
        self.device = None
        self.action = None
        self.led = None

        self.hex = None
        self.red = None
        self.green = None
        self.blue = None
        self.speed = None
        self.repeat = None
        self.wave = None
        self.pattern = None

    def hex_to_rgb(self, value): # http://stackoverflow.com/a/214657
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

    def setup_device(self):
        self.device = usb.core.find(idVendor=0x04d8, idProduct=0xf372)

        # Device found?
        if self.device is None:
            raise ValueError('Device not found')

        # Linux kernel sets up a device driver for USB device, which you have to detach.
        # Otherwise trying to interact with the device gives a 'Resource Busy' error.
        try:
          self.device.detach_kernel_driver(0)
        except Exception as e:
          pass

        self.device.set_configuration()

    def close_device(self):
        if self.device :
            self.device.close()
            self.device = None

    def write_value(self, values ):
        self.device.write(1, values)
        self.device.write(1, values) # Run it again to ensure it works.

    def turn_off(self):
        self.write_value( [1, 0, 255, 0, 0, 0, 0])

    def turn_on(self):
        self.write_value( [1, 255, 255, 255, 255, 0, 0])

    def set_pattern(self):
        self.write_value( [6,self.pattern,self.repeat,0,0,0,0] )

    def set_wave(self):
        self.write_value( [4,self.wave,self.red,self.green,self.blue,0,self.repeat,self.speed] )

    def set_strobe(self):
        self.write_value( [3,self.led,self.red,self.green,self.blue,self.speed,0,self.repeat] )

    def set_fade(self):
        self.write_value( [2,self.led,self.red,self.green,self.blue,self.speed,0] )

    def set_color(self):
        self.write_value( [1,self.led,self.red,self.green,self.blue,0,0] )

    def set_color_rgb(self, red, green, blue, led=255):
        self.write_value( [1, led, red, green, blue, 0, 0])

#
# def main():
#     global RED
#     global GREEN
#     global BLUE
#     setup_device()
#     setup_args()
#
#     if HEX:
#         rgb = hex_to_rgb(HEX)
#         RED = rgb[0]
#         GREEN = rgb[1]
#         BLUE = rgb[2]
#
#     # Determine which action
#     if ACTION == 'color':
#         set_color()
#     elif ACTION == 'fade':
#         set_fade()
#     elif ACTION == 'strobe':
#         set_strobe()
#     elif ACTION == 'wave':
#         set_wave()
#     elif ACTION == 'pattern':
#         set_pattern()
#
# def setup_args():
#     global RED
#     global GREEN
#     global BLUE
#     global SPEED
#     global REPEAT
#     global WAVE
#     global PATTERN
#     global ACTION
#     global LED
#     global HEX
#
#     parser = initArgParser()
#     args = parser.parse_args()
#
#     ACTION  = args.action if args.action else 'color'
#     RED     = args.r if args.r else 0
#     GREEN   = args.g if args.g else 0
#     BLUE    = args.b if args.b else 0
#     SPEED   = args.s if args.s else 0
#     REPEAT  = args.t if args.t else 0
#     WAVE    = args.w if args.w else 0
#     PATTERN = args.p if args.p else 0
#     LED     = args.l if args.l else 255
#     HEX     = args.x if args.x else 0

# def initArgParser():
#
#     # Setup argument parser
#     parser = argparse.ArgumentParser(description='Luxafor Arguments')
#     parser.add_argument('action', help='Action', choices=["color", "fade", "wave", "strobe", "pattern"])
#     parser.add_argument('-l', help='LED', type=int)
#     parser.add_argument('-b', help='Blue Value', type=int)
#     parser.add_argument('-r', help='Red Value', type=int)
#     parser.add_argument('-g', help='Green Value', type=int)
#     parser.add_argument('-s', help='Speed Value', type=int)
#     parser.add_argument('-t', help='Repeat Value', type=int)
#     parser.add_argument('-w', help='Wave Value', type=int)
#     parser.add_argument('-p', help='Pattern Value', type=int)
#     parser.add_argument('-x', help='Hex Color', type=str)
#
#     return parser
#
# if __name__ == '__main__':
#     main()