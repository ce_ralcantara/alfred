from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QThread
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QShortcut, QMainWindow, QApplication
import sys
import os
import platform

import alfred

import datetime
import time
import random

import argparse
import usb.core

# def resource_path(relative_path):
#     """ Get absolute path to resource, works for dev and for PyInstaller """
#     try:
#         # PyInstaller creates a temp folder and stores path in _MEIPASS
#         base_path = getattr(sys, '_MEIPASS', os.getcwd())
#     except Exception:
#         base_path = os.path.abspath(".")
#
#     return os.path.join(base_path, relative_path)

class AlfredThread(QThread):
    update_timers = pyqtSignal()
    update_app_state = pyqtSignal()
    update_gui = pyqtSignal()

    def __init__(self, alfred_app):
        QThread.__init__(self)
        self.alfred = alfred_app

    def run(self):
        while self.isRunning() :
            if self.alfred :
                self.update_timers.emit()
                self.update_gui.emit()
                if self.alfred.app_state != AlfredApp.WAITING :
                    self.update_app_state.emit()
                time.sleep(0.1)

class AppTimer(object):
    WAITING = 'Waiting'
    RUNNING = 'Running'
    INTERRUPTED = 'Stopped'
    ENDED = 'Ended'

    def __init__(self, timer_name, total_time=1):
        self.id = timer_name
        self.total_time = total_time
        self.reset_timer()

    def reset_timer(self):
        self.start_time = 0
        self.end_time = 0
        self.minutes_left = 0
        self.seconds_left = 0
        self.progress_percentage = 0.0

        self.state = AppTimer.WAITING

    def set_timer(self, total_time):
        self.total_time = total_time
        self.reset_timer()

    def get_state(self):
        return self.state

    def get_progress_percentage(self):
        return self.progress_percentage

    def start_timer(self):
        if self.state != AppTimer.RUNNING :
            self.reset_timer()
            self.start_time = datetime.datetime.now()
            self.end_time = self.start_time + datetime.timedelta(minutes=self.total_time)
            self.state = AppTimer.RUNNING

    def update_timer(self):
        if self.state == AppTimer.RUNNING :
            current_time = datetime.datetime.now()

            self.minutes_left = 0
            self.seconds_left = 0
            if current_time >= self.end_time :
                self.state = AppTimer.ENDED
            else :
                time_cleared = current_time - self.start_time
                time_from_start = self.end_time - self.start_time
                self.time_left = time_from_start - time_cleared

                self.minutes_left = self.time_left.seconds/60
                self.seconds_left = self.time_left.seconds%60

                self.progress_percentage = 100 - ((float(time_cleared.total_seconds()) / float(time_from_start.total_seconds())) * 100.0)
                self.progress_percentage = min(self.progress_percentage, 100.0)

    def stop_timer(self):
        self.state = AppTimer.INTERRUPTED

class AlfredApp(QMainWindow, alfred.Ui_Dialog):
    WAITING = 'waiting'
    WORKING = 'working'
    INTERRUPTED = 'interrupted'
    SUMMARIZING = 'summarizing'
    LONELY = 'lonely'
    TOGGLE_BUSY = 'toggle'

    # SETUP COLORS
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)

    LUXAFOR_RED = 82
    LUXAFOR_GREEN = 71

    colour_codes = {'green': 71, 'yellow': 89, 'red': 82, 'blue': 66, 'white': 87, 'off': 79}

    def __init__(self, parent=None):
        super(AlfredApp, self).__init__(parent)
        self.setupUi(self)

        # need to implement class handler to return object
        # should create a common luxafor interface/object
        # should create a common instance function that returns the correct interface (cython-hidapi vs. PyUsb)
        # luxafor.setup_device()
        self.luxafor = self.get_luxafor()
        self.setup_device()

        self.app_state = AlfredApp.WAITING

        self.available_color = AlfredApp.GREEN
        self.available_luxafor = AlfredApp.LUXAFOR_GREEN

        self.busy_color = AlfredApp.RED
        self.busy_luxafor = AlfredApp.LUXAFOR_RED

        # reset internal variables
        self.main_timer = AppTimer('main_timer', 25) #length of each pomodoro
        self.summary_timer = AppTimer('summary_timer', 5) #length of time to summarize pomodoro
        self.lonely_timer = AppTimer('lonely_timer', 5) #alftred becomes lonely after this amount of time

        # Alfred Toggle
        self.toggle_shortcut = QShortcut(QKeySequence("Ctrl+Alt+A"), self)
        self.toggle_shortcut.activated.connect(self.alfred_toggle)

        # alfred timer thread
        self.alfred_thread = AlfredThread(self)

        self.alfred_thread.update_timers.connect(self.update_timers)
        self.alfred_thread.update_app_state.connect(self.update_app_state)
        self.alfred_thread.update_gui.connect(self.update_gui)

        self.alfred_thread.start()

        # connect the button functionality
        self.timer_btn_start.clicked.connect(self.start_button_click)
        self.timer_btn_stop.clicked.connect(self.stop_button_click)

        self.change_state(AlfredApp.WAITING)

    @pyqtSlot()
    def on_toggle(self):
        self.alfred_toggle()

    def closeEvent(self, QCloseEvent):
        if self.luxafor :
            self.luxafor.close_device()

    def get_luxafor(self):
        dev = usb.core.find(idVendor=0x04d8, idProduct=0xf372)
        if dev is None:
            print('Not connected')
            return

        try:
            dev.detach_kernel_driver(0)
        except usb.core.USBError:
            pass

        return dev

    def setup_device(self):
        self.luxafor.set_configuration()
        self.luxafor.write(1, [0, 0])
        self.luxafor.write(1, [0, self.LUXAFOR_GREEN])

    def alfred_toggle(self):
        if self.app_state == AlfredApp.WAITING or \
           self.app_state == AlfredApp.INTERRUPTED :
            self.change_state(AlfredApp.TOGGLE_BUSY)
        elif self.app_state == AlfredApp.TOGGLE_BUSY :
            self.change_state(AlfredApp.WAITING)

    def change_state(self, new_state):
        self.app_state = new_state
        if new_state == AlfredApp.TOGGLE_BUSY :
            self.set_light_busy()

            self.title_message.setText(self.get_motivational_starting_message())
            self.alfred_toggle_focused.show()

            self.title_message.show()# self.luxafor.turn_on()

            self.timer_remaining_time.hide()
            self.time_select.hide()

            self.timer_btn_start.hide()
            self.timer_btn_stop.hide()

            self.timer_interrupted.hide()
            self.timer_progress_bar.hide()
        elif new_state == AlfredApp.WAITING :
            self.set_light_busy(False)

            self.main_timer.stop_timer()
            self.summary_timer.stop_timer()

            self.main_timer.reset_timer()
            self.summary_timer.reset_timer()

            self.title_message.setText("Select time, and start your task!")
            self.timer_remaining_time.setText('00:00')
            self.timer_btn_start.setText('Start')

            self.time_select.show()
            self.timer_btn_start.show()
            self.timer_btn_stop.hide()
            self.timer_remaining_time.hide()
            self.timer_interrupted.hide()

            self.alfred_toggle_shortcut.show()
            self.alfred_toggle_focused.hide()

            self.timer_progress_bar.show()
            self.timer_progress_bar.setValue(0)
            self.timer_progress_bar.setTextVisible(False)
        elif new_state == AlfredApp.WORKING :
            self.set_light_busy()

            # set the timer
            self.main_timer.set_timer(self.time_select.value())
            self.main_timer.reset_timer()

            # update the remaining time and progression bar
            progress_percentage = 100.0 - self.main_timer.progress_percentage
            self.timer_progress_bar.setValue(progress_percentage)
            self.timer_remaining_time.setText('{}:00'.format(self.main_timer.total_time))

            # setup the motivational message!
            self.title_message.setText(self.get_motivational_starting_message())

            # start the timer
            self.main_timer.start_timer()

            self.title_message.show()
            self.timer_remaining_time.show()
            self.timer_btn_stop.show()
            self.timer_btn_start.hide()
            self.time_select.hide()
            self.timer_interrupted.hide()

            self.alfred_toggle_shortcut.hide()
        elif new_state == AlfredApp.INTERRUPTED :
            # set the timer
            self.main_timer.stop_timer()
            self.set_light_busy(False)

            # setup the motivational message!
            self.title_message.setText(self.get_motivational_refocus_message())
            self.timer_btn_start.setText('Try Again')
            self.timer_interrupted.show()

            self.title_message.show()
            self.timer_remaining_time.show()
            self.alfred_toggle_shortcut.show()

            self.timer_btn_start.show()
            self.timer_btn_stop.hide()

            self.timer_progress_bar.hide()
            self.timer_remaining_time.hide()
            self.time_select.hide()
        elif new_state == AlfredApp.LONELY :
            self.time_select.show()
            self.timer_btn_start.show()
            self.timer_btn_stop.hide()
            self.timer_remaining_time.hide()

            self.alfred_toggle_shortcut.show()
            self.alfred_toggle_focused.hide()

            self.timer_progress_bar.hide()

    def set_light_busy(self, busy = True):
        if busy == True :
            self.update_window_palette(self.busy_color[0], self.busy_color[1], self.busy_color[2])
            self.luxafor.write(1, [0, self.busy_luxafor])
        else :
            self.update_window_palette(self.available_color[0], self.available_color[1], self.available_color[2])
            self.luxafor.write(1, [0, self.available_luxafor])

    # TIMER + PROGRESS BAR RELATED LOGIC
    def start_button_click(self):
        if self.app_state == AlfredApp.WAITING :
            self.change_state(AlfredApp.WORKING)
        elif self.app_state == AlfredApp.INTERRUPTED :
            self.change_state(AlfredApp.WAITING)
        elif self.app_state == AlfredApp.LONELY :
            self.change_state(AlfredApp.WAITING)

    def stop_button_click(self):
        if self.app_state == AlfredApp.WORKING :
            self.change_state(AlfredApp.INTERRUPTED)
        elif self.app_state == AlfredApp.SUMMARIZING :
            self.change_state(AlfredApp.WAITING)

    # UPDATE TIMER
    def update_timers(self):
        self.main_timer.update_timer()
        self.summary_timer.update_timer()
        # self.lonely_timer.update_timer()

    def update_app_state(self):
        if self.app_state == AlfredApp.WAITING :
            pass
            # if self.lonely_timer.state == AppTimer.ENDED :
            #     self.change_state(AlfredApp.LONELY)
        elif self.app_state == AlfredApp.WORKING :
            if self.main_timer.state == AppTimer.ENDED :
                self.change_state(AlfredApp.WAITING)
        elif self.app_state == AlfredApp.SUMMARIZING :
            if self.summary_timer.state == AppTimer.ENDED :
                self.change_state(AlfredApp.WAITING)

    def update_gui(self):
        if self.app_state == AlfredApp.WAITING :
            pass
        elif self.app_state == AlfredApp.WORKING :
            minutes_left = int(round(self.main_timer.minutes_left,0))
            seconds_left = int(round(self.main_timer.seconds_left, 0))

            if seconds_left < 10 :
                self.timer_remaining_time.setText('{}:0{}'.format(minutes_left, seconds_left))
            else :
                self.timer_remaining_time.setText('{}:{}'.format(minutes_left, seconds_left))

            self.timer_progress_bar.setValue(self.main_timer.progress_percentage)
            self.timer_progress_bar.update()
        elif self.app_state == AlfredApp.SUMMARIZING :
            minutes_left = int(round(self.summary_timer.minutes_left,0))
            seconds_left = int(round(self.summary_timer.seconds_left, 0))

            if seconds_left < 10 :
                self.timer_remaining_time.setText('{}:0{}'.format(minutes_left, seconds_left))
            else :
                self.timer_remaining_time.setText('{}:{}'.format(minutes_left, seconds_left))

            self.timer_progress_bar.setValue(self.summary_timer.progress_percentage)
            self.timer_progress_bar.update()

    # MOTIVATIONAL MESSAGES
    def get_motivational_starting_message(self):
        messages = [
            "Your time is now! Go go go!",
            "Keep it up champ! Good job!",
            "A pomodoro a day, keeps OT away.",
            "It takes on average 15 minutes to focus.",
            "Rising up to the challenge of our rivals!",
            "Your tasks just got Sho-ryu-kened!",
        ]

        return random.choice(messages)

    def get_motivational_refocus_message(self):
        messages = [
            "Interruptions are hard to avoid.",
            "Failing is growth. Try again!",
            "Your focus and time is invaluable.",
        ]

        return random.choice(messages)

    def update_window_palette(self, red, green, blue):
        palette = QtGui.QPalette()
        role = QtGui.QPalette.Background
        palette.setColor(role, QtGui.QColor(red, green, blue))
        self.setPalette(palette)


def main():
    if getattr(sys, 'frozen', False):
        path = getattr(sys, '_MEIPASS', os.getcwd())
        os.chdir(path)

    app = QApplication(sys.argv)
    form = AlfredApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()